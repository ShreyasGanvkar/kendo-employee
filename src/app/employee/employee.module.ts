import { NotificationModule } from "@progress/kendo-angular-notification";
import { ViewComponent } from "./view/view.component";
import { NgModule, LOCALE_ID } from "@angular/core";
import { CommonModule } from "@angular/common";

import { EmployeeRoutingModule } from "./employee-routing.module";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Ng2SearchPipeModule } from "ng2-search-filter";

import { GridModule } from "@progress/kendo-angular-grid";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { IndexComponent } from "./index/index.component";
import { WindowModule, DialogsModule } from "@progress/kendo-angular-dialog";
import { LayoutModule } from "@progress/kendo-angular-layout";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { LabelModule } from "@progress/kendo-angular-label";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { IntlModule } from "@progress/kendo-angular-intl";
import { UploadsModule } from "@progress/kendo-angular-upload";

@NgModule({
  declarations: [IndexComponent, ViewComponent, CreateComponent, EditComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    GridModule,
    ButtonsModule,
    DropDownsModule,
    WindowModule,
    DialogsModule,
    LayoutModule,
    InputsModule,
    LabelModule,
    NotificationModule,
    DateInputsModule,
    IntlModule,
    UploadsModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "en-AU" }],
})
export class EmployeeModule {}
