import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import {
  DragResizeService,
  WindowComponent,
} from "@progress/kendo-angular-dialog";
import { offset } from "@progress/kendo-popup-common";

import { EmployeeService } from "../employee.service";
import { Employee } from "../employee";

@Component({
  selector: "app-view",
  templateUrl: "./view.component.html",
  styleUrls: ["./view.component.css"],
})
export class ViewComponent implements OnInit {
  id?: number;
  employee?: Employee;
  @Output() close: EventEmitter<any> = new EventEmitter();

  ESC_KEY = 27;
  width = "640";
  height = "420";
  minHeight = "210";
  minWidth = "200";
  title = "Employee Details";

  @Input() empID = "";

  @ViewChild(WindowComponent, { static: false })
  windowComponent: WindowComponent;

  /**
   * Centers Kendo-Window to the middle of the screen
   */
  @HostListener("window:scroll", [])
  @HostListener("window:resize", [])
  centerWindow() {
    const windowService = this.windowComponent["service"] as DragResizeService;

    if (!windowService) {
      return;
    }
    if (windowService.options.state === "maximized") {
      return;
    }

    const wnd = windowService.windowViewPort;
    const wrapper = offset(windowService.window.nativeElement);

    const top = Math.max(0, (wnd.height - wrapper.height) / 2);
    const left = Math.max(0, (wnd.width - wrapper.width) / 2);

    windowService.options.position = "fixed";
    this.windowComponent.setOffset("top", top);
    this.windowComponent.setOffset("left", left);
  }

  @HostListener("keydown", ["$event"])
  public onComponentKeydown(event: KeyboardEvent): void {
    // don't fix .keyCode to keep IE11 support
    // tslint:disable-next-line: deprecation
    if (event.keyCode === this.ESC_KEY) {
      this.windowComponent.close.emit();
    }
  }

  constructor(
    public employeeService: EmployeeService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let id = this.empID;

    // console.log("ViewComponent:ngOnInit():id", id);
    this.employeeService.find(id).subscribe((data: Employee) => {
      // console.log("ViewComponent:ngOnInit():data", data);
      this.employee = data;
      // this.windowComponent.close.emit();
    });
  }
}
