import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  LOCALE_ID,
  Inject,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { DataBindingDirective } from "@progress/kendo-angular-grid";
import { process } from "@progress/kendo-data-query";

import { Employee } from "../employee";
import { EmployeeService } from "../employee.service";

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.css"],
})
export class IndexComponent implements OnInit {
  @ViewChild(DataBindingDirective, { static: true })
  dataBinding: DataBindingDirective;
  @ViewChild("template", { read: TemplateRef, static: true }) // for notification
  public notificationTemplate: TemplateRef<any>;

  empID: number;

  createWindowOpen: boolean = false;
  viewWindowOpen: boolean = false;
  editWindowOpen: boolean = false;

  employees: Employee[] = [];

  public gridData: any[]; // data for manipulation
  public gridView: any[]; // data displayed in the grid

  public mySelection: string[] = [];

  // public openedRouterWindow = false;
  public openedDeleteConfDialog = false;
  private deleteID: number; //id for deleting a record

  loading: boolean = true;

  constructor(
    public employeeService: EmployeeService,
    private router: Router,
    // private notificationService: NotificationService,
    public route: ActivatedRoute,
    @Inject(LOCALE_ID) public locale: string
  ) {
    // console.error("IndexComponent:constructor()");
  }

  ngOnInit(): void {
    // console.error("IndexComponent:ngOnInit()");
    this.employeeService.getAll().subscribe((data: Employee[]) => {
      // console.error("IndexComponent: ngOnInit():data", data);
      // console.error(
      //   "IndexComponent:ngOnInit():this.gridView----Before",
      //   this.gridView
      // );
      this.gridData = data;
      this.gridView = data;
      this.loading = false;
      //  console.error("IndexComponent:ngOnInit():this.gridData",this.gridData);
      // console.error(
      //   "IndexComponent:ngOnInit():this.gridView------After",
      //   this.gridView
      // );
    });
  }

  //Search box filtering
  public onFilter(inputValue: string): void {
    // console.error("IndexComponent:onFilter():inputValue",inputValue);
    this.gridView = process(this.gridData, {
      filter: {
        logic: "or",
        filters: [
          {
            field: "id",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "empFname",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "empLname",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "empDOB",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "deptID",
            operator: "contains",
            value: inputValue,
          },
        ],
      },
    }).data;
    // console.error("IndexComponent:onFilter():this.gridView",this.gridView);
    this.dataBinding.skip = 0;
  }
  ///

  //Modal window employee-details/add-form/edit-form
  // public openRouterWindow() {
  //   this.openedRouterWindow = true;
  // }

  // public closeRouterWindow() {
  //   this.openedRouterWindow = false;
  // }
  ///

  //Delete
  deleteEmp() {
    let id = this.deleteID;
    // console.error("IndexComponent:deleteEmp():id", id);
    this.employeeService.delete(id).subscribe((res) => {
      // console.error("IndexComponent:deleteEmp():res", res);
      this.gridView = this.gridView.filter((item) => item.id !== id);
      // console.error("IndexComponent:deleteEmp():this.gridView", this.gridView);
      this.closeDeleteConfDialog();
      this.router.navigateByUrl("/employee/index");
      this.employeeService.showSuccess("Record deleted successfully.");
    });
  }
  ///

  //Delete confirmation dialog
  public openDeleteConfDialog(id) {
    // console.error("IndexComponent:openDeleteConfDialog():id", id);
    this.deleteID = id;
    // console.error(
    //   "IndexComponent:openDeleteConfDialog():this.deleteID",
    //   this.deleteID
    // );
    this.openedDeleteConfDialog = true;
  }

  public closeDeleteConfDialog() {
    this.openedDeleteConfDialog = false;
  }
  ///

  iclose() {
    console.error("IndexComponent:close() ............................");

    this.createWindowOpen = false;

    this.getEmployeesAfterClosingWindow();
  }

  closeViewWindow() {
    this.viewWindowOpen = false;
  }

  closeEditWindow() {
    this.editWindowOpen = false;
    this.getEmployeesAfterClosingWindow();
  }

  getEmployeesAfterClosingWindow() {
    this.employeeService.getAll().subscribe((data: Employee[]) => {
      // console.error("IndexComponent: ngOnInit():data", data);
      this.gridData = data;
      this.gridView = data;
      //  console.error("IndexComponent:ngOnInit():this.gridData",this.gridData);
      // console.error("IndexComponent:ngOnInit():this.gridView", this.gridView);
    });
  }
}
